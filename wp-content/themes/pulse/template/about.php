<?php 
/*
Template Name: About Page
*/
get_header(); ?>
<div class="main_content">
    <div class="main_content_bg">
      <img src="<?php echo get_template_directory_uri(); ?>/images/content-bg.jpg" alt="">
    </div>

    <!-- about section -->
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <section class="about_section layout_padding">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="img-box">
              <?php the_post_thumbnail(); ?>
            </div>
          </div>
          <div class="col-md-6">
            <div class="detail-box">
              <div class="heading_container">
                <h2>
                  <?php the_title(); ?>
                </h2>
              </div>
              <?php the_content(); ?>             
            </div>
          </div>
        </div>
      </div>
    </section>
    <?php endwhile; else : ?>
      <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
    <?php endif; ?>
    <!-- end about section -->

  </div>
<?php get_footer(); ?>