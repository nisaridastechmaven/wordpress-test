<?php 
/*
Template Name: Service Page
*/
get_header(); ?>
<div class="main_content">
    <div class="main_content_bg">
      <img src="images/content-bg.jpg" alt="">
    </div>

    <!-- service section -->

    <section class="service_section layout_padding">
      <div class="container py_mobile_45">
        <div class="heading_container heading_center">
          <h2> Our Services </h2>
        </div>
        <div class="row">

          <?php query_posts( 'category_name=services&order=ASC' ); ?>
          <?php if ( have_posts() ): ?>
          <?php while ( have_posts() ) : the_post(); ?>
            <div class="col-md-4">
              <div class="box ">
                <div class="img-box">
                  <?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
                </div>
                <div class="detail-box">
                  <h5>
                    <?php the_title(); ?>
                  </h5>
                  <?php the_content(); ?>
                </div>
              </div>
            </div>
          <?php endwhile; else: ?>
          <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
          <?php endif; ?>         

        </div>
      </div>
    </section>

    <!-- end service section -->

  </div>
  <?php get_footer(); ?>




