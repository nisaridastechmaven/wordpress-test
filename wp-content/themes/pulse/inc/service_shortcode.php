<?php
$html = '';
$query = new WP_Query( 'category_name=services&order=ASC' );
if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();
$html .= '<div class="col-md-4">';
$html .= '<div class="box ">';
$html .= '<div class="img-box">';
if ( has_post_thumbnail() ) {
	$image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'single-post-thumbnail' );
	$html .= '<img src="'.$image[0].'" alt="">';
}
$html .= '</div>';
$html .= '<div class="detail-box">';
$html .= '<h5>'.get_the_title().'</h5>';
$html .= '<p>'.get_the_content().'</p>';
$html .= '</div>';
$html .= '</div>';
$html .= '</div>';
endwhile; 
wp_reset_postdata();
else :
$html .= '<p>Sorry, no posts matched your criteria</p>';
endif;
return $html;