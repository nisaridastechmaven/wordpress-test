<!DOCTYPE html>
<html>

<head>
  <!-- Basic -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <!-- Mobile Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <!-- Site Metas -->
  <meta name="keywords" content="" />
  <meta name="description" content="" />
  <meta name="author" content="" />
  <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.png" type="image/x-icon">
  
  <?php wp_head(); ?>
</head>

<body <?php body_class();?>>
  <div class="hero_area ">
    <div class="hero_bg_box">
      <?php if ( !is_front_page() ) : ?>
        <img src="<?php echo get_template_directory_uri(); ?>/images/hero-bg.jpg" alt="">
      <?php else: ?>
        <img src="<?php echo get_field( "slider_image" )['url']; ?>" alt="">
      <?php endif; ?>
    </div>
    <!-- header section strats -->
    <header class="header_section">
      <div class="container-fluid">
        <nav class="navbar navbar-expand-lg custom_nav-container">
          <a class="navbar-brand" href="<?php echo home_url(); ?>">
            <span>
              Piuse
            </span>
          </a>
          <div class="" id="">
            <div class="User_option">
              <form class="form-inline my-2  mb-3 mb-lg-0">
                <input type="search" placeholder="Search" />
                <button class="btn   my-sm-0 nav_search-btn" type="submit"> <i class="fa fa-search" aria-hidden="true"></i> </button>
              </form>
            </div>

            <div class="custom_menu-btn">
              <button onclick="openNav()">
                <span class="s-1"> </span>
                <span class="s-2"> </span>
                <span class="s-3"> </span>
              </button>
            </div>
            <div id="myNav" class="overlay">
              <?php 
              wp_nav_menu( array(
              'theme_location' => 'primary_menu',
              'container' => '',
              'items_wrap'     => '<div class="overlay-content">%3$s</div>'
              ));
              ?>
            </div>
          </div>
        </nav>
      </div>
    </header>
    <!-- end header section -->

    <?php if ( is_front_page() ) : ?>
    <?php
      $slider_title = get_field( "slider_title" );
      $slider_content = get_field( "slider_content" );
    ?>  
    <!-- slider section -->
    <section class="slider_section">
      <div class="container">
        <div class="row">
          <div class="col-md-6 ">
            <div class="detail-box">
              <h1>
                <?php echo $slider_title; ?>
              </h1>
              <p>
                <?php echo $slider_content; ?>
              </p>
              <a href="">
                Read More
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- end slider section -->
  <?php endif; ?>
</div>