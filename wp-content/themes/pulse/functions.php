<?php 
// INCLUDE STYLES AND SCRIPTS
function load_scripts() {
 
    wp_enqueue_style ( 'pulse_bootstrap_css', get_template_directory_uri().'/css/bootstrap.css');
    wp_enqueue_style ( 'pulse_owl_css', 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css');
    wp_enqueue_style ( 'pulse_custom_css', get_template_directory_uri().'/css/style.css');
    wp_enqueue_style ( 'pulse_responsive_css', get_template_directory_uri().'/css/responsive.css');


    wp_enqueue_script( 'jquery');
    wp_enqueue_script( 'pulse_bootstrap_js',get_template_directory_uri().'/js/bootstrap.js', array(), time() );
    wp_enqueue_script( 'pulse_owl_js','https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js', array(), time() );
    wp_enqueue_script( 'pulse_custom_js',get_template_directory_uri().'/js/custom.js', array(), time() );
 
}
add_action('wp_enqueue_scripts', 'load_scripts');

add_theme_support( 'title-tag' );
    
add_theme_support( 'post-thumbnails' );
 
function pulse_register_nav_menu(){
    register_nav_menus( array(
        'primary_menu' => __( 'Primary Menu', 'pulse' ),
        'footer_menu'  => __( 'Footer Menu', 'pulse' ),
    ) );
}
add_action( 'after_setup_theme', 'pulse_register_nav_menu', 0 );

add_filter( 'body_class', 'add_custom_css' );
function add_custom_css( $classes ) {
    if ( !is_front_page() ) :
        $classes[] = 'sub_page';
    endif;
    return $classes;
}

add_shortcode('services_shortcode','services_shortcode_callback');
function services_shortcode_callback(){
    return require_once get_template_directory().'/inc/service_shortcode.php';
}