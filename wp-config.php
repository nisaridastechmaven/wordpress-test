<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress-test' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'J;q&3sW~CYE#,K/z|zS(pi][7s~<WQWoB^UR#Rhd W<v0OBr~2M9r8qk@Y:2L-C(' );
define( 'SECURE_AUTH_KEY',  '6^cYVqq2e7cwW5Z-:IkyrREd Ep!SINMqU*VQvV.CeblZM{ktvDPHD@pvwo{;%rD' );
define( 'LOGGED_IN_KEY',    'DVB.4gl)Vp~U<>KBR5zWfRVco%C+8.kY6WsW!lKEAk&)`)8A{F[Pc=b(b.>?KI+B' );
define( 'NONCE_KEY',        '7:y`c*wD/y${77XlFI=A(.RmG;SG8z#YEy?_(N+! X0{<vWniwk`GSmv##xcv@yu' );
define( 'AUTH_SALT',        '-lZ&%2Fv%2]:/kpZbE%KkT<1}ZM7_J!NqtTy5dYd==~y]%+/sWqAMu64 jwWg!^L' );
define( 'SECURE_AUTH_SALT', 'm^Zqn1-AD=,%W0sfHaeOQT::r shHvH:){@h]B@qrF:eEmg6/nfMjmb;QU0iqd33' );
define( 'LOGGED_IN_SALT',   '9lYaMgg-evXxy3[{Mo(m.Y=_dE~E?tix2ndT^(p1 vN$.ELX3@aecX0-@F73~f=_' );
define( 'NONCE_SALT',       'dH.=s%3Pp ;&poNga@2D=^q+W$7LLA{3 E/lf1ZD@~^g:w6mg#Cs,_QnZvGpcZDG' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
